package com.st.peliculas.PeluculasFinal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PeluculasFinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(PeluculasFinalApplication.class, args);
	}

}
