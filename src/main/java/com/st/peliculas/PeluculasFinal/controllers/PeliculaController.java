package com.st.peliculas.PeluculasFinal.controllers;

import com.st.peliculas.PeluculasFinal.models.entity.Pelicula;
import com.st.peliculas.PeluculasFinal.models.service.IPeliculaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/pelicula")
public class PeliculaController {

    @Autowired
    IPeliculaService peliculaService;

    @GetMapping("/peliculas")
    public List<Pelicula> getPeliculas(){
        return peliculaService.findAll();
    }

    @GetMapping("/peliculas/page/{page}")
    public Page<Pelicula> getPeliculasPage(@PathVariable Integer page){
        Pageable pageable = PageRequest.of(page, 5);
        return peliculaService.findAll(pageable);
    }

    @GetMapping("/peliculas/{id}")
    public ResponseEntity<?> getPelicula(@PathVariable Long id){
        return peliculaService.findById(id);
    }

    @PostMapping("/peliculas")
    public ResponseEntity<?> create(@Valid @RequestBody Pelicula pelicula, BindingResult result){
        Map<String, Object> response = new HashMap<>();
        if(result.hasErrors()){
            List<String> errors = new ArrayList<>();
            for (FieldError err: result.getFieldErrors()) {
                errors.add("El campo '" + err.getField() + "'" + err.getDefaultMessage());
            }
            response.put("errors", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        return peliculaService.save(pelicula);
    }

    @PutMapping("/peliculas/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody Pelicula pelicula, BindingResult result, @PathVariable Long id){
        Map<String, Object> response = new HashMap<>();
        if(result.hasErrors()){
            List<String> errors = new ArrayList<>();
            for (FieldError err: result.getFieldErrors()) {
                errors.add("El campo '" + err.getField() + "'" + err.getDefaultMessage());
            }
            response.put("errors", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        return peliculaService.update(pelicula, id);
    }

    @DeleteMapping("/peliculas/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> delete(@PathVariable Long id){
        return peliculaService.delete(id);
    }

    @GetMapping("/peliculas/filtro/{filtro}")
    public List<Pelicula> seach(@PathVariable String filtro){
        return peliculaService.seach(filtro);
    }
}
