package com.st.peliculas.PeluculasFinal.models.service;

import com.st.peliculas.PeluculasFinal.models.dao.IPeliculaDao;
import com.st.peliculas.PeluculasFinal.models.entity.Pelicula;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductoImpl implements IPeliculaService{
    @Autowired
    private IPeliculaDao peliculaDao;
    @Override
    public List<Pelicula> findAll() {
        return (List<Pelicula>) peliculaDao.findAll();
    }

    @Override
    public Page<Pelicula> findAll(Pageable pageable) {
        return peliculaDao.findAll(pageable);
    }

    @Override
    public ResponseEntity<?> findById(Long id) {
        Map<String, Object> response = new HashMap<>();
        Pelicula pelicula=null;
        try{
            pelicula = peliculaDao.findById(id).orElse(null);
        }catch (DataAccessException e){
            response.put("message", "Error en la base de datos");
            response.put("error", e.getMessage().concat(e.getMostSpecificCause().getMessage()));
            return  new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (pelicula==null){
            response.put("message", "La pelicula con el id: " + id + " no se encuentra en la base de datos");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        response.put("message", "Pelicula encontrada con exito");
        response.put("pelicula",pelicula);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> save(Pelicula pelicula) {
        Map<String, Object> response = new HashMap<>();
        Pelicula peliculaS=null;
        try{
            peliculaS = peliculaDao.save(pelicula);
        }catch (DataAccessException e){
            response.put("message", "Error al crear la pelicula");
            response.put("Error", e.getMessage().concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if(peliculaS==null){
            response.put("message", "No se logro registrar la pelicula en la base de datos");
            return new ResponseEntity<>(response,HttpStatus.NO_CONTENT);
        }
        response.put("message", "Pelicula registrada con exito");
        response.put("pelicula", peliculaS);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> update(Pelicula pelicula, Long id) {
        Map<String, Object> response = new HashMap<>();
        Pelicula peliculaActual = peliculaDao.findById(id).orElse(null);
        Pelicula peliculaActualizada = null;

        if(peliculaActual==null){
            response.put("message", "La pelicula con el id: " + id + " no se encuentra en la base de datos");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        try{
            peliculaActual.setName(pelicula.getName());
            peliculaActual.setUpdateAt(new Date());
            peliculaActual.setDescription(pelicula.getDescription());
            peliculaActual.setClassification(pelicula.getClassification());
            peliculaActual.setYear(pelicula.getYear());
            peliculaActual.setMonth(pelicula.getMonth());
            peliculaActualizada = peliculaDao.save(peliculaActual);
        }catch (DataAccessException e){
            response.put("message", "No se logro actualizar la pelicula");
            response.put("Error", e.getMessage().concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("message", "Pelicula actualizada con exito");
        response.put("pelicula", peliculaActualizada);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> delete(Long id) {
        Map<String, Object> response = new HashMap<>();
        Pelicula peliculaD = peliculaDao.findById(id).orElse(null);

        if(peliculaD==null){
            response.put("message", "La pelicula con el id: " + id + " no se encuentra en la base de datos");
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        try{
            peliculaDao.deleteById(id);
        }catch (DataAccessException e){
            response.put("message", "No se logro eliminar la pelicula de la base de datos");
            response.put("Error", e.getMessage().concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("message", "Pelicula eliminada con exito");
        return new ResponseEntity<>(response,HttpStatus.OK);
    }

    @Override
    public List<Pelicula> seach(String filtro) {
        //return peliculaDao.seachNative(filtro);
        return peliculaDao.search(filtro);
        //return peliculaDao.findByNameContainingOrYearContaining(filtro, filtro);
    }
}
