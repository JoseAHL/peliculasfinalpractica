package com.st.peliculas.PeluculasFinal.models.dao;

import com.st.peliculas.PeluculasFinal.models.entity.Pelicula;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IPeliculaDao extends JpaRepository<Pelicula, Long> {
    List<Pelicula> findByNameContaining(String name);

    @Query(value = "select p from Pelicula p where p.name like %:filtro%")
    List<Pelicula> search(@Param("filtro") String filtro);

    @Query(
            value = "select * from peliculas where peliculas.name like %:filtro%",
            nativeQuery = true
    )
    List<Pelicula> seachNative(@Param("filtro") String filtro);
}
