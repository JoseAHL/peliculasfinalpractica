package com.st.peliculas.PeluculasFinal.models.service;

import com.st.peliculas.PeluculasFinal.models.entity.Pelicula;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IPeliculaService {

    List<Pelicula> findAll();
    Page<Pelicula> findAll(Pageable pageable);
    ResponseEntity<?> findById(Long id);
    ResponseEntity<?> save(Pelicula pelicula);
    ResponseEntity<?> update(Pelicula pelicula, Long id);
    ResponseEntity<?> delete(Long id);
    List<Pelicula> seach(String filtro);
}
