package com.st.peliculas.PeluculasFinal.models.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "peliculas")
public class Pelicula implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min = 3, max = 30, message = "El tamaño del nombre debe de estar entre 3 y 30 caracteres")
    @NotEmpty(message = "El nombre no puede estar vacio")
    @Column(nullable = false)
    private String name;

    @PrePersist //realiza acciones antes de que se inserte en la base de datos
    public  void prePersis(){
        createAt = new Date();
        updateAt = new Date();
    }

    //@NotNull(message = "La fecha no debe de estar vacia")
    @Column(name = "create_at")
    @Temporal(TemporalType.DATE)
    private Date createAt;

    /*@PrePersist //realiza acciones antes de que se inserte en la base de datos
    public  void prePersis2(){
        updateAt = new Date();
    }*/

    //@NotNull(message = "La fecha no debe de estar vacia")
    @Column(name = "update_at")
    @Temporal(TemporalType.DATE)
    private Date updateAt;

    @Size(min = 1, max = 300, message = "La descripcion debe estar entre un rango de 1 a 300 caracteres")
    @NotEmpty(message = "La descripcion no puede estar vacia")
    @Column(nullable = false)
    private String description;

    @Size(min = 1, max = 30, message = "La clasificacion debe estar entre un rango de 1 a 30 caracteres")
    @NotEmpty(message = "La clasificacion no puede estar vacia")
    @Column(nullable = false)
    private String classification;

    @Column(nullable = false)
    private int year;

    @NotEmpty(message = "El mes no pued estar vacio")
    @Column(nullable = false)
    private String month;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    private static final Long serialVersionUID= 1L;


}
